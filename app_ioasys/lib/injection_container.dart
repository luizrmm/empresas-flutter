import 'package:app_ioasys/features/authentication/data/datasources/authentication_remote_data_source.dart';
import 'package:app_ioasys/features/authentication/data/repositories/authentication_repository_impl.dart';
import 'package:app_ioasys/features/authentication/domain/repositories/authentication_repository.dart';
import 'package:app_ioasys/features/authentication/domain/usecases/authenticate.dart';
import 'package:app_ioasys/features/authentication/presentation/bloc/authentication_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;

final sl = GetIt.instance;
Future<void> init() async {
  //! Features - Authenticate
  // Bloc
  sl.registerFactory(
    () => AuthenticationBloc(authenticate: sl()),
  );

  //UseCases
  sl.registerLazySingleton(
    () => AuthenticateUseCase(sl()),
  );

  // Repository
  sl.registerLazySingleton<AuthenticationRepository>(
    () => AuthenticationRepositoryImpl(remoteDataSource: sl()),
  );

  //DataSources
  sl.registerLazySingleton<AuthenticationRemoteDataSource>(
    () => AuthenticationRemoteDataSourceImpl(client: sl()),
  );

  //! Core

  //! External
  sl.registerLazySingleton(() => http.Client());
}
