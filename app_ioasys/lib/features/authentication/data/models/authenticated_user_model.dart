import 'package:app_ioasys/features/authentication/domain/entities/authentication.dart';
import 'package:flutter/foundation.dart';

class AuthenticatedUserModel extends AuthenticatedUser {
  AuthenticatedUserModel(
      {@required int id, @required String investorName, @required email})
      : super(id: id, investorName: investorName, email: email);

  factory AuthenticatedUserModel.fromJson(Map<String, dynamic> json) {
    return AuthenticatedUserModel(
        id: json['investor']['id'],
        investorName: json['investor']['investor_name'],
        email: json['investor']['email']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['investor_name'] = this.investorName;
    data['email'] = this.email;
    return data;
  }
}
