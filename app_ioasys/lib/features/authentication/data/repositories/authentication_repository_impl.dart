import 'package:app_ioasys/core/error/exceptions.dart';
import 'package:app_ioasys/features/authentication/data/datasources/authentication_remote_data_source.dart';
import 'package:app_ioasys/features/authentication/domain/entities/authentication.dart';
import 'package:app_ioasys/core/error/failures.dart';
import 'package:app_ioasys/features/authentication/domain/repositories/authentication_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';

class AuthenticationRepositoryImpl implements AuthenticationRepository {
  final AuthenticationRemoteDataSource remoteDataSource;

  AuthenticationRepositoryImpl({@required this.remoteDataSource});
  @override
  Future<Either<Failure, AuthenticatedUser>> authenticate(
      String email, String password) async {
    try {
      final authenticatedUser =
          await remoteDataSource.authenticate(email, password);
      return Right(authenticatedUser);
    } on AuthenticationException {
      return Left(AuthenticationFailure());
    }
  }
}
