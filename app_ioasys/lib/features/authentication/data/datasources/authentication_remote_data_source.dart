import 'dart:convert';

import 'package:app_ioasys/core/error/exceptions.dart';
import 'package:app_ioasys/features/authentication/data/models/authenticated_user_model.dart';
import 'package:flutter/foundation.dart';

import 'package:http/http.dart' as http;

abstract class AuthenticationRemoteDataSource {
  /// Calls the authentication endpoint.
  ///
  /// Throws a [ServerException] for all error codes.
  Future<AuthenticatedUserModel> authenticate(String email, String password);
}

class AuthenticationRemoteDataSourceImpl
    implements AuthenticationRemoteDataSource {
  final http.Client client;

  AuthenticationRemoteDataSourceImpl({@required this.client});
  @override
  Future<AuthenticatedUserModel> authenticate(
      String email, String password) async {
    final response = await client.post(
      'https://empresas.ioasys.com.br/api/v1/users/auth/sign_in',
      headers: {
        'Content-Type': 'application/json',
      },
      body: jsonEncode({"email": email, "password": password}),
    );
    if (response.statusCode == 200) {
      return AuthenticatedUserModel.fromJson(json.decode(response.body));
    } else {
      throw AuthenticationException();
    }
  }
}
