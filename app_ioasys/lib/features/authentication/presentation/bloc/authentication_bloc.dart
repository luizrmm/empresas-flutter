import 'dart:async';

import 'package:app_ioasys/core/error/failures.dart';
import 'package:app_ioasys/features/authentication/domain/entities/authentication.dart';
import 'package:app_ioasys/features/authentication/domain/usecases/authenticate.dart';
import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

part 'authentication_event.dart';
part 'authentication_state.dart';

const String AUTHENTICATION_FAILURE_MESSAGE = 'Credenciais incorretas';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final AuthenticateUseCase authenticate;
  AuthenticationBloc({@required this.authenticate})
      : super(AuthenticationInitial());

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    yield AuthenticationLoading();
    if (event is GetAuthenticate) {
      final failureOrAuthenticatedUser = await authenticate(
          Params(email: event.email, password: event.password));
      yield* _eitherLoadedOrErrorState(failureOrAuthenticatedUser);
    }
  }

  Stream<AuthenticationState> _eitherLoadedOrErrorState(
    Either<Failure, AuthenticatedUser> failureOrAuthenticatedUser,
  ) async* {
    yield failureOrAuthenticatedUser.fold(
      (failure) => AuthenticationError(message: _mapFailureToMessage(failure)),
      (authenticatedUser) =>
          AuthenticationLoaded(authenticatedUser: authenticatedUser),
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case AuthenticationFailure:
        return AUTHENTICATION_FAILURE_MESSAGE;
      default:
        return 'Unexpected error';
    }
  }
}
