part of 'authentication_bloc.dart';

abstract class AuthenticationEvent extends Equatable {
  const AuthenticationEvent();

  @override
  List<Object> get props => [];
}

class GetAuthenticate extends AuthenticationEvent {
  final String email;
  final String password;

  GetAuthenticate(this.email, this.password);
}
