import 'package:app_ioasys/features/company/presentation/pages/companies_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:app_ioasys/features/authentication/presentation/bloc/authentication_bloc.dart';
import 'package:app_ioasys/features/authentication/presentation/widgets/widgets.dart';
import 'package:app_ioasys/injection_container.dart';

class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF5F5F5),
      body: SingleChildScrollView(child: buildBody(context)),
    );
  }

  BlocProvider<AuthenticationBloc> buildBody(BuildContext context) {
    return BlocProvider(
      create: (_) => sl<AuthenticationBloc>(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          LoginHeader(),
          SizedBox(
            height: 60,
          ),
          BlocConsumer<AuthenticationBloc, AuthenticationState>(
            listener: (context, state) {
              if (state is AuthenticationLoaded) {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => CompaniesPage()),
                );
              }
            },
            builder: (context, state) {
              if (state is AuthenticationInitial) {
                return FormBody();
              } else if (state is AuthenticationError) {
                return FormBody(
                  message: state.message,
                );
              } else if (state is AuthenticationLoading) {
                return CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Color(0xFFE01E69)),
                );
              }
              return FormBody();
            },
          ),
        ],
      ),
    );
  }
}
