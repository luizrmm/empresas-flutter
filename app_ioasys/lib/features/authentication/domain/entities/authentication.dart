import 'package:equatable/equatable.dart';

class AuthenticatedUser extends Equatable {
  final int id;
  final String investorName;
  final String email;

  AuthenticatedUser({this.id, this.investorName, this.email});
  @override
  List<Object> get props => [id, investorName, email];
}
