import 'package:app_ioasys/core/error/failures.dart';
import 'package:app_ioasys/features/authentication/domain/entities/authentication.dart';
import 'package:dartz/dartz.dart';

abstract class AuthenticationRepository {
  Future<Either<Failure, AuthenticatedUser>> authenticate(
      String email, String password);
}
