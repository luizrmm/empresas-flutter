import 'package:app_ioasys/core/error/failures.dart';
import 'package:app_ioasys/core/usecases/usecase.dart';
import 'package:app_ioasys/features/authentication/domain/entities/authentication.dart';
import 'package:app_ioasys/features/authentication/domain/repositories/authentication_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

class AuthenticateUseCase implements UseCase<AuthenticatedUser, Params> {
  final AuthenticationRepository repository;
  AuthenticateUseCase(this.repository);
  @override
  Future<Either<Failure, AuthenticatedUser>> call(Params params) async {
    return await repository.authenticate(params.email, params.password);
  }
}

class Params extends Equatable {
  final String email;
  final String password;

  Params({@required this.email, @required this.password});

  @override
  List<Object> get props => [email, password];
}
