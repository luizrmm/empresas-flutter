import 'package:app_ioasys/core/error/failures.dart';
import 'package:app_ioasys/features/authentication/domain/entities/authentication.dart';
import 'package:app_ioasys/features/authentication/domain/usecases/authenticate.dart';
import 'package:app_ioasys/features/authentication/presentation/bloc/authentication_bloc.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockAuthenticate extends Mock implements AuthenticateUseCase {}

void main() {
  MockAuthenticate mockAuthenticate;

  setUp(() {
    mockAuthenticate = MockAuthenticate();
  });
  test('initialState should be AuthenticationInitial', () {
    //arrange
    final bloc = AuthenticationBloc(authenticate: mockAuthenticate);
    // assert
    expect(bloc.state, equals(AuthenticationInitial()));
    bloc.close();
  });

  group('AuthenticateUser', () {
    final int tId = 1;
    final String tEmail = 'test@email.com';
    final String tInvestorName = 'investor name';

    final authenticatedUser =
        AuthenticatedUser(id: tId, email: tEmail, investorName: tInvestorName);

    blocTest(
      'should emit [loading, loaded] from authenticate use case',
      build: () {
        when(mockAuthenticate(any))
            .thenAnswer((_) async => Right(authenticatedUser));
        return AuthenticationBloc(authenticate: mockAuthenticate);
      },
      act: (bloc) => bloc.add(GetAuthenticate('test@email', 'testPass')),
      expect: [
        AuthenticationLoading(),
        AuthenticationLoaded(authenticatedUser: authenticatedUser),
      ],
    );

    blocTest(
      'should emit [loading, error] from authenticate use case',
      build: () {
        when(mockAuthenticate(any))
            .thenAnswer((_) async => Left(AuthenticationFailure()));
        return AuthenticationBloc(authenticate: mockAuthenticate);
      },
      act: (bloc) => bloc.add(GetAuthenticate('test@email', 'testPass')),
      expect: [
        AuthenticationLoading(),
        AuthenticationError(message: AUTHENTICATION_FAILURE_MESSAGE),
      ],
    );
  });
}
