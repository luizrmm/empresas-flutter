import 'package:app_ioasys/features/authentication/domain/entities/authentication.dart';
import 'package:app_ioasys/features/authentication/domain/repositories/authentication_repository.dart';
import 'package:app_ioasys/features/authentication/domain/usecases/authenticate.dart';
import 'package:dartz/dartz.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';

class MockAuthenticationRepository extends Mock
    implements AuthenticationRepository {}

void main() {
  AuthenticateUseCase usecase;
  MockAuthenticationRepository mockAuthenticationRepository;

  setUp(() {
    mockAuthenticationRepository = MockAuthenticationRepository();
    usecase = AuthenticateUseCase(mockAuthenticationRepository);
  });

  final int tId = 1;
  final String tInvestorName = 'test name';
  final String tEmail = 'test@test.com';
  final String tPassword = 'testpassword';

  final authenticatedUser =
      AuthenticatedUser(id: tId, investorName: tInvestorName, email: tEmail);

  test('should return a authenticated user for the repository', () async {
    // arrange
    when(mockAuthenticationRepository.authenticate(any, any))
        .thenAnswer((_) async => Right(authenticatedUser));
    //act
    final result = await usecase(Params(email: tEmail, password: tPassword));
    //assert
    expect(result, Right(authenticatedUser));
    verify(mockAuthenticationRepository.authenticate(tEmail, tPassword));
    verifyNoMoreInteractions(mockAuthenticationRepository);
  });
}
