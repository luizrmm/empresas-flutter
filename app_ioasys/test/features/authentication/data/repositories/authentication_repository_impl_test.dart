import 'package:app_ioasys/core/error/exceptions.dart';
import 'package:app_ioasys/core/error/failures.dart';
import 'package:app_ioasys/features/authentication/data/datasources/authentication_remote_data_source.dart';
import 'package:app_ioasys/features/authentication/data/models/authenticated_user_model.dart';
import 'package:app_ioasys/features/authentication/data/repositories/authentication_repository_impl.dart';
import 'package:dartz/dartz.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';

class MockAuthenticationRemoteDataSource extends Mock
    implements AuthenticationRemoteDataSource {}

void main() {
  AuthenticationRepositoryImpl repository;
  MockAuthenticationRemoteDataSource mockRemoteDataSource;

  setUp(() {
    mockRemoteDataSource = MockAuthenticationRemoteDataSource();
    repository =
        AuthenticationRepositoryImpl(remoteDataSource: mockRemoteDataSource);
  });

  group('Authenticate', () {
    final String tEmail = 'test@email.com';
    final String tPassword = 'testPassword';

    final tAuthenticatedUserModel = AuthenticatedUserModel(
        id: 1, investorName: 'test name', email: 'test@email.com');
    test(
        'should return remote data when the call to remote data source is successful',
        () async {
      // arrange
      when(mockRemoteDataSource.authenticate(any, any))
          .thenAnswer((_) async => tAuthenticatedUserModel);
      // act
      final result = await repository.authenticate(tEmail, tPassword);
      // assert
      verify(mockRemoteDataSource.authenticate(tEmail, tPassword));
      expect(result, equals(Right(tAuthenticatedUserModel)));
    });

    test(
        'should throw a Authentication Failure when the call to remote data source is unuccessful',
        () async {
      // arrange
      when(mockRemoteDataSource.authenticate(any, any))
          .thenThrow(AuthenticationException());
      // act
      final result = await repository.authenticate(tEmail, tPassword);
      // assert
      verify(mockRemoteDataSource.authenticate(tEmail, tPassword));

      expect(result, equals(Left(AuthenticationFailure())));
    });
  });
}
