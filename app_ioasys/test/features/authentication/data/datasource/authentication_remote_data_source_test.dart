import 'dart:convert';

import 'package:app_ioasys/core/error/exceptions.dart';
import 'package:app_ioasys/features/authentication/data/datasources/authentication_remote_data_source.dart';
import 'package:app_ioasys/features/authentication/data/models/authenticated_user_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/mockito.dart';

import '../../../../fixtures/fixture_reader.dart';

class MockHttpCliente extends Mock implements http.Client {}

void main() {
  AuthenticationRemoteDataSourceImpl remoteDataSource;
  MockHttpCliente mockHttpCliente;

  setUp(() {
    mockHttpCliente = MockHttpCliente();
    remoteDataSource =
        AuthenticationRemoteDataSourceImpl(client: mockHttpCliente);
  });

  void setUpMockHttpClientSuccess200() {
    when(mockHttpCliente.post(any,
            headers: anyNamed('headers'), body: anyNamed('body')))
        .thenAnswer((_) async =>
            http.Response(fixture('authenticated_user.json'), 200));
  }

  group('AuthenticateUser', () {
    final String tEmail = 'testeapple@ioasys.com.br';
    final String tPassword = '12341234';

    final tAuthenticatedUserModel = AuthenticatedUserModel.fromJson(
        jsonDecode(fixture('authenticated_user.json')));
    test(
        'should perform a post resquest on a url with the credentials to endpoint',
        () {
      // arrange
      setUpMockHttpClientSuccess200();
      // act
      remoteDataSource.authenticate(tEmail, tPassword);
      // assert
      verify(
        mockHttpCliente.post(
          'https://empresas.ioasys.com.br/api/v1/users/auth/sign_in',
          headers: {
            'Content-Type': 'application/json',
          },
          body: jsonEncode(
              {"email": "testeapple@ioasys.com.br", "password": "12341234"}),
        ),
      );
    });

    test(
        'should return a AuthenticatedUser when the response code is 200(success)',
        () async {
      // arrange
      setUpMockHttpClientSuccess200();
      // act
      final result = await remoteDataSource.authenticate(tEmail, tPassword);
      print(result.toJson());
      // assert
      expect(result, equals(tAuthenticatedUserModel));
    });
    test('should throw a AuthenticationException when request url fails',
        () async {
      // arrange
      when(mockHttpCliente.post(any,
              headers: anyNamed('headers'), body: anyNamed('body')))
          .thenAnswer((_) async => http.Response('Something went wrong', 401));
      // act
      final call = remoteDataSource.authenticate;
      // assert
      expect(() => call(tEmail, tPassword),
          throwsA(isA<AuthenticationException>()));
    });
  });
}
