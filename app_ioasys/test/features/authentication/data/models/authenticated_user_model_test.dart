import 'dart:convert';

import 'package:app_ioasys/features/authentication/data/models/authenticated_user_model.dart';
import 'package:app_ioasys/features/authentication/domain/entities/authentication.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../../fixtures/fixture_reader.dart';

void main() {
  final tAuthenticatedModel = AuthenticatedUserModel(
      id: 1, investorName: 'test name', email: 'test@email.com');

  test('should be a subclass of AuthenticatedUser entity', () {
    //assert
    expect(tAuthenticatedModel, isA<AuthenticatedUser>());
  });

  group('fromJson', () {
    test('should return a valid model when the json is a user', () {
      // arrange
      final Map<String, dynamic> jsonMap =
          json.decode(fixture('authenticated_user.json'));
      // act
      final result = AuthenticatedUserModel.fromJson(jsonMap);
      // assert
      expect(result, tAuthenticatedModel);
    });
  });

  group('toJson', () {
    test(
      'should return a JSON map containing the proper data',
      () async {
        // act
        final result = tAuthenticatedModel.toJson();
        // assert
        final expectedMap = {
          "id": 1,
          "investor_name": 'test name',
          "email": 'test@email.com'
        };
        expect(result, expectedMap);
      },
    );
  });
}
